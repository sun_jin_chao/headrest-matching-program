import wx

# 初始化数据
touzhen_info = {
    "13856419-00": {"7007100-SA3DY": "0"},
    "13856421-00": {"7007100-SA3ADY": "0"},
    "15628030-00": {"7007100-SA3AYAP": "0"},
    "16117639-00": {"7007100-SA3ADY": "0"},
    "13856413-00": {"7007100-SA3DY": "7007200-SA3DY"},
    "13856388-00": {"7007100-SA3ADY": "7007200-SA3ADY"},
    "14069536-00": {"7007100-SA3ADY": "7007200-SA3ADY"},
    "15628028-00": {"7007100-SA3AYAP": "7007200-SA3AYAP"},
    "16117637-00": {"7007100-SA3ADY": "7007200-SA3ADY"},
    "16147474-00": {"7308210A-SK200HAV": "0"},
    "16147473-00": {"7308210-SK200WAW": "0"},
    "13397740-00": {"7308210-SK2-A": "0"},
    "15387055-00": {"7308210-SK200WAW": "0"},
    "16147483-00": {"7308210A-SK200HAV": "7308120-SK2-A"},
    "16147484-00": {"7308210-SK200WAW": "7308120-SK200WAW"},
    "13397737-00": {"7308210-SK2-A": "7308120-SK2-A"},
    "15387053-00": {"7308210-SK200WAW": "7308120-SK200WAW"},
}


class MyFrame(wx.Frame):
    def __init__(self, parent, title):
        super(MyFrame, self).__init__(parent, title=title, size=(826, 685))
        self.SetBackgroundColour(wx.WHITE)
        # 创建标题
        self.Title_Txt = wx.StaticText(
            self, wx.ID_ANY, "座椅头枕匹配", wx.DefaultPosition, wx.DefaultSize, 0
        )
        self.Title_Txt.SetFont(
            wx.Font(
                80,
                wx.FONTFAMILY_DEFAULT,
                wx.FONTSTYLE_NORMAL,
                wx.FONTWEIGHT_BOLD,
                False,
                wx.EmptyString,
            )
        )
        # 创建输入框
        self.text_ctrl = wx.TextCtrl(self, style=wx.TE_PROCESS_ENTER)
        self.text_ctrl.SetMaxLength(111)
        self.text_ctrl.SetFont(
            wx.Font(
                50,
                wx.FONTFAMILY_DEFAULT,
                wx.FONTSTYLE_NORMAL,
                wx.FONTWEIGHT_NORMAL,
                False,
                wx.EmptyString,
            )
        )
        self.Bind(wx.EVT_TEXT_ENTER, self.on_text_enter, self.text_ctrl)

        # 创建重置按钮
        self.reset_button = wx.Button(self, label="重置", size=(150, 80))
        self.reset_button.SetFont(
            wx.Font(
                20,
                wx.FONTFAMILY_DEFAULT,
                wx.FONTSTYLE_NORMAL,
                wx.FONTWEIGHT_NORMAL,
                False,
                wx.EmptyString,
            )
        )
        self.Bind(wx.EVT_BUTTON, self.on_reset, self.reset_button)

        # 创建状态显示标签
        self.status_label = wx.StaticText(self, label="")
        self.status_label.SetFont(
            wx.Font(
                100,
                wx.FONTFAMILY_DEFAULT,
                wx.FONTSTYLE_NORMAL,
                wx.FONTWEIGHT_NORMAL,
                False,
                wx.EmptyString,
            )
        )

        # 布局控件
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.Title_Txt, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 10)
        sizer.Add(self.text_ctrl, 0, wx.EXPAND | wx.ALL, 10)
        sizer.Add(self.reset_button, 0, wx.CENTER | wx.ALL, 10)
        sizer.Add(self.status_label, 0, wx.EXPAND | wx.ALL, 10)
        self.SetSizer(sizer)

        # 初始化状态变量
        self.stage = (
            0  # 当前阶段：0-等待输入整椅号，1-等待输入边头枕号，2-等待输入中头枕号
        )
        self.chair_id = None  # 当前输入的整椅号
        self.pillow_id = None  # 当前输入的边头枕号

    def on_text_enter(self, event):
        input_data = self.text_ctrl.GetValue()
        self.text_ctrl.Clear()  # 清空输入框

        if self.stage == 0:
            # 检查输入的整椅号
            if input_data in touzhen_info:
                self.chair_id = input_data
                self.status_label.SetLabel("OK!请继续扫描边头枕号")
                self.status_label.SetForegroundColour(wx.GREEN)
                self.stage = 1
            else:
                self.status_label.SetLabel("NG! 整椅号错误")
                self.status_label.SetForegroundColour(wx.RED)
                self.reset()

        elif self.stage == 1:
            # 检查输入的边头枕号
            if input_data in touzhen_info[self.chair_id]:
                self.pillow_id = input_data
                value = touzhen_info[self.chair_id][self.pillow_id]
                if value == "0":
                    self.status_label.SetLabel("OK")
                    self.status_label.SetForegroundColour(wx.GREEN)
                    self.reset()
                else:
                    self.status_label.SetLabel("OK!请继续输入中头枕数据")
                    self.status_label.SetForegroundColour(wx.GREEN)
                    self.stage = 2
            else:
                self.status_label.SetLabel("NG! 边头枕号错误")
                self.status_label.SetForegroundColour(wx.RED)
                self.reset()

        elif self.stage == 2:
            # 检查输入的中头枕号
            if input_data == touzhen_info[self.chair_id][self.pillow_id]:
                self.status_label.SetLabel("OK")
                self.status_label.SetForegroundColour(wx.GREEN)
                self.reset()
            else:
                self.status_label.SetLabel("NG! 中头枕号错误")
                self.status_label.SetForegroundColour(wx.RED)
                self.reset()

    def on_reset(self, event):
        self.reset()

    def reset(self):
        self.stage = 0
        self.chair_id = None
        self.pillow_id = None
        self.text_ctrl.SetLabel("")
        # self.status_label.SetLabel("")
        self.text_ctrl.SetFocus()  # 设置焦点到输入框，方便用户继续输入


# 创建应用并显示主窗口
app = wx.App(False)
frame = MyFrame(None, "头枕号验证系统")
frame.Show()
app.MainLoop()
